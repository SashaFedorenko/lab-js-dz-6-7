import { AbstractControl, ValidationErrors } from "@angular/forms";

export const AgeValidator = (allowZero = false) => (control: AbstractControl): ValidationErrors | null => {
  const age: number = control.value;
  const error = {
    age: 'Age should be a positive integare'
  }
  if (!Number.isInteger(age) && age <0) {
    return error;
  }

  if (!allowZero) {
    return error;
  }

  return null;
}
